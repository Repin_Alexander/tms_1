a = {'a': 1, 'b': 2, 'c': 3}
b = {'c': 3, 'd': 4, 'e': 5}
ab = {}

for key in {**a, **b}.keys():
    new_item = list([a.setdefault(key), b.setdefault(key)])
    ab[key] = new_item

print(ab)
