from string import digits


def validate_choice(value):
    if value not in '12345':
        print('Некорректный ввод\n')
        return False
    else:
        return True


def validate_value(value1, value2):
    for i in value1 + value2:
        if i not in digits:
            print('Некорректный ввод\n')
            return False
    return True


def summ(a, b):
    return a + b


def mult(a, b):
    return a * b


def dif(a, b):
    return a - b


def div(a, b):
    c = str(a / b)
    ls = c.split('.')
    return int(ls[0]), int(ls[1][0])


while True:
    choice = input('1. Сложение\n'
                   '2. Вычитание\n'
                   '3. Умножение\n'
                   '4. Деление\n'
                   '5. Выход')

    if validate_choice(choice):
        if choice == '5':
            break
        a = input('Enter int')
        b = input('Enter int')
        if validate_value(a, b):

            a = int(a)
            b = int(b)
            if choice == '1':
                print(summ(a, b))
            if choice == '2':
                print(dif(a, b))
            if choice == '3':
                print(mult(a, b))
            if choice == '4':
                quotient, reminder = div(a, b)
                print(f'Частное: {quotient}\n'
                      f'Остаток: {reminder}')
