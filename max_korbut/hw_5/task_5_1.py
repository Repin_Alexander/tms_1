"""Быки коровы (ранчо)"""
import random

"""Загадываем число"""
hidden_number = random.sample('0123456789', 4)
hidden_number = ''.join(hidden_number)
print(hidden_number)
"""Пытаемся его отгадать"""
while True:
    number = str(input("Попытайтесь отгадать число: "))
    if hidden_number != number:
        """Узнаем одинаковые номера"""
        identical_numbers = []
        for i in number:
            if i in hidden_number:
                identical_numbers.append(i)
        """Узнаем кол-во быков и коров"""
        bulls = []
        cows = []
        for i in identical_numbers:
            index_hidden_number = hidden_number.index(i)
            index_number = number.index(i)
            if index_hidden_number == index_number:
                bulls.append(i)
            else:
                cows.append(i)
        """Корректно выводим кол-во коров и быков игроку"""
        if len(bulls) < 2:
            if len(cows) < 2:
                print(f"{len(bulls)} bull and {len(cows)} cow!")
            elif len(cows) >= 2:
                print(f"{len(bulls)} bull and {len(cows)} cows!")
        elif len(bulls) >= 2:
            if len(cows) < 2:
                print(f"{len(bulls)} bulls and {len(cows)} cow!")
            elif len(cows) >= 2:
                print(f"{len(bulls)} bulls and {len(cows)} cows!")
        continue
    else:
        """
        Если число угадано, то игрок молодец и может взять пирожок,
        а цикл прерывается
        """
        print(f"Вы откадали число, это было {hidden_number}")
        break
