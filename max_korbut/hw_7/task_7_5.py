"""шифр цезаря + бонусные очки"""


def en_de_code(text: str, number_indents: int, language: str, operation: int):
    """Шифрует и расшифровывает сообщение по заданному параметру"""

    en_lower = "abcdefghijklmnopqrstuvwxyz"
    en_upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    rus_lower = "абвгдежзийклмнопрстуфхцчшщъыьэюя"
    rus_upper = "АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"
    exceptions = [',', '.', ':', ';', '+', '-', '?', '!', ' ',
                  '0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
    new_text = []
    for symbol in text:
        if symbol in exceptions:
            new_text.append(symbol)
        else:
            if operation == 1:
                if language == "English":
                    if symbol in en_lower:
                        index_symbol = en_lower.index(symbol)
                        new_index = index_symbol + number_indents
                        if new_index > 25:
                            new_index -= 26
                        new_symbol = en_lower[new_index]
                        new_text.append(new_symbol)
                    elif symbol in en_upper:
                        index_symbol = en_upper.index(symbol)
                        new_index = index_symbol + number_indents
                        if new_index > 25:
                            new_index -= 26
                        new_symbol = en_upper[new_index]
                        new_text.append(new_symbol)
                elif language == "Русский":
                    if symbol in rus_lower:
                        index_symbol = rus_lower.index(symbol)
                        new_index = index_symbol + number_indents
                        if new_index > 31:
                            new_index -= 32
                        new_symbol = rus_lower[new_index]
                        new_text.append(new_symbol)
                    elif symbol in rus_upper:
                        index_symbol = rus_upper.index(symbol)
                        new_index = index_symbol + number_indents
                        if new_index > 31:
                            new_index -= 32
                        new_symbol = rus_upper[new_index]
                        new_text.append(new_symbol)
            elif operation == 2:
                if language == "English":
                    if symbol in en_lower:
                        index_symbol = en_lower.index(symbol)
                        new_index = index_symbol - number_indents
                        new_symbol = en_lower[new_index]
                        new_text.append(new_symbol)
                    elif symbol in en_upper:
                        index_symbol = en_upper.index(symbol)
                        new_index = index_symbol - number_indents
                        new_symbol = en_upper[new_index]
                        new_text.append(new_symbol)
                elif language == "Русский":
                    if symbol in rus_lower:
                        index_symbol = rus_lower.index(symbol)
                        new_index = index_symbol - number_indents
                        new_symbol = rus_lower[new_index]
                        new_text.append(new_symbol)
                    elif symbol in rus_upper:
                        index_symbol = rus_upper.index(symbol)
                        new_index = index_symbol - number_indents
                        new_symbol = rus_upper[new_index]
                        new_text.append(new_symbol)
    new_text = ''.join(new_text)
    print(new_text)


def language_definition(text):
    """Определяте язык текста"""

    en = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
    rus = 'абвгдежзийклмнопрстуфхцчшщъыьэюяАБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ'
    if text[0] in en:
        return "English"
    elif text[0] in rus:
        return "Русский"


def start():
    """Запрашивает у пользователя текст и количество отступов"""

    text = input("Введите текст для его последующей (рас)шифровки: ")
    number_indents = int(input("Введите количество отступов: "))
    print("Какую операцию вы хотите выполнить?"
          "\n1. Шифровка\n2. Расшифровка")
    operation = int(input("Введите цифру выбранной операции: "))
    language = language_definition(text)
    if operation == 1:
        print(f"Вы выбрали операцию 'Шифровка', язык '{language}'")
    elif operation == 2:
        print(f"Вы выбрали операцию 'Расшифровка', язык '{language}'")
    en_de_code(text, number_indents, language, operation)


if __name__ == "__main__":
    start()
