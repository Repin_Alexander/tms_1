"""матрицы"""


def matrix(input_list: list, n_str: int, n_symbols: int):
    """Принемает на вход данные и возвращает матрицу"""

    lower_len = 0
    upper_len = n_symbols
    for i in range(0, n_str):
        text_matrix = input_list[lower_len:upper_len]
        print(text_matrix)
        lower_len += n_symbols
        upper_len += n_symbols


matrix([1, 2, 3, 4, 5, 6], 2, 3)
print("--------------------")
matrix([1, 2, 3, 4, 5, 6, 7, 8], 4, 2)
print("--------------------")
matrix([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 5, 2)
