"""Объединить словари по ключам, значения поместить в список"""
a = {
    'a': 1,
    'b': 2,
    'c': 3,
}
b = {
    'c': 3,
    'd': 4,
    'e': 5,
}

"""Сделаем копии словарей и объединим их"""
a_kop = a.copy()
b_kop = b.copy()
a_kop.update(b_kop)
"""Получим список ключей"""
keys = []
for key in a_kop.keys():
    keys.append(key)
"""
Создаем новый словарь, перебором вытягиваем значения из словаря а и b,
формируем списки со значениями и добавляем их вновый словарь
"""
aa = {}
for key in keys:
    a_value = a.get(key)
    b_value = b.get(key)
    if a_value is None:
        values = [None, b_value]
    elif b_value is None:
        values = [a_value, None]
    else:
        values = [a_value, b_value]
    aa[key] = values
print(aa)
