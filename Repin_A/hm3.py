# Заменить символ “#” на символ “/” в строке 'www.my_site.com#about'
s1='www.my_site.com#about'
print(s1.replace("#","/"))

#Напишите программу, которая добавляет ‘ing’ к словам
s2=["asd", "dsa", "qwe"]
print (["{}ing".format(s) for s in s2])

#В строке “Ivanou Ivan” поменяйте местами слова => "Ivan Ivanou"

s3="Ivanou Ivan"
s4=s3.split(" ")
s4.reverse()
print(" ".join(s4))

#Напишите программу которая удаляет пробел в начале, в конце строки
s6=" hello moto "
if s6[0]==" ":
    s6= s6[1:]
if s6[len(s6)-1]==" ":
    s6 = s6[:len(s6)-1]

print (s6)

#Напишите программу которая удаляет пробел в начале, в конце строки
print (s6.strip())
