#Примеры:
#likes() -> "no one likes this"
#likes("Ann") -> "Ann likes this"
#likes("Ann", "Alex") -> "Ann and Alex like this"
#likes("Ann", "Alex", "Mark") -> "Ann, Alex and Mark like this"
#likes("Ann", "Alex", "Mark", "Max") -> "Ann, Alex and 2 others like this"

name_list=["Ann", "Alex", "Mark", "Max"]

if len(name_list)<1:
    likes = "no one"
if len(name_list)==1:
    likes = name_list[0]
if len(name_list)==2:
    likes = " and ".join(name_list)
else:
    likes = f"{name_list[0]}, {name_list[1]} and "
    if len(name_list)==3:
        likes += name_list[2]
    else:
        likes+= str(len(name_list)-2)+" others"
print(f"{likes} likes this")
