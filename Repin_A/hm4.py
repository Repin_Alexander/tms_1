#1. Перевести строку в массив

str1="Robin Singh"
print(str1.split(" "))
str2="I love arrays they are my favorite"
print(str2.split(" "))

#2
str3="Minsk"
str4="Belarus"
list=["Ivan", "Ivanou"]
print(f"Привет, {list [0]} {list [1]}! Добро пожаловать в {str3} {str4}")
#" ".join(list)

#3 list==>str
list2= ["I", "love", "arrays", "they", "are", "my", "favorite"]
print(" ".join(list2))

#4 Создайте список из 10 элементов, вставьте
# на 3-ю позицию новое значение, удалите элемент из списка под индексом 6

list3 = [0,1,2,3,4,5,6,7,8,9]
print(f"до изменений :{list3}")
list3.insert(3, 10)
del list3[6]
print(f"после изменений :{list3}")

#5 Есть 2 словаря
a = { 'a': 1, 'b': 2, 'c': 3}
b = { 'c': 3, 'd': 4,'e': 5}
c = a.copy()
c.update(b)
print(c)
for k in c.keys():
    value=[None, None]
    if k in a:
        value[0]=a[k]
    if k in b:
        value[1]=b[k]
    c[k]=value
#    c[k] = [a[k] if k in a else None, b[k] if k in b else None]
print(c)








