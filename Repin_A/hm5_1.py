answer = 3219
digits=[int(x)for x in str(answer)]
a = None

while a != answer:
    a = int(input("enter the number: "))
    if a<1000 or a>9999:
        print("error")
    guess=[int(x)for x in str(a)]
    cows, bulls = 0, 0
    for i, d in enumerate(guess):
        if digits[i]==d:
            bulls+=1
        elif d in digits:
            cows+=1
    print(f"guessed {cows} cows and {bulls} bulls")
print("you win!")