a = 1

while a != 101:
    b = a % 3
    c = a % 5
    if b != 0 and c != 0:
        print(a)
        a += 1
    elif b == 0 and c != 0:
        print('Fuzz')
        a += 1
    elif b != 0 and c == 0:
        print('Buzz')
        a += 1
    elif b == 0 and c == 0:
        print('FuzzBuzz')
        a += 1
