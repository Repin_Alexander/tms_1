# Перевести строку в массив
# "Robin Singh" => ["Robin”, “Singh"]
# "I love arrays they are my favorite" =>
# ["I", "love", "arrays", "they", "are", "my", "favorite"]

name = "Robin Singh"
mas_name = name.split()
print(mas_name)

mas_lov = "I love arrays they are my favorite"
new_mas_lov = mas_lov.split()
print(new_mas_lov)
