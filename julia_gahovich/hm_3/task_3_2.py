# Напишите программу, которая добавляет ‘ing’ к словам


# вариант 1
def adding_func(words):
    for word in words:
        word = str(word) + 'ing'
        print(word)


words = ['cat', 'dog', 'mouse']
adding_func(words)


# вариант 2
txt = 'cat dog mouse'

txt = txt.split()
txt_ing = 'ing '.join(txt) + 'ing'

print(txt_ing)
