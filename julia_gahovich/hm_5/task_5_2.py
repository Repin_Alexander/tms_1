"""Создайте программу, которая, принимая массив имён, возвращает строку
 описывающая количество лайков (как в Facebook). Функция работает на
 нескольких языках - язык ответа определяется по языку входного массива."""
from translate import Translator
from langdetect import detect_langs

# Examples for names(Different languages)
# names = []
# names = ['Andreas']
# names = ['Катя', 'Сергей']
# names = ['Ann', 'Jimmy', 'Bob']
# names = ['Marie', 'Jurgen', 'Wolfgang', 'Ursula']
names = ['Pietro', 'Madonna', 'Giovanni', 'Maria']
sms = ''
name = ' '.join(names)


def lang_detection():
    language = ''
    if name != '':
        list_of_languages = detect_langs(name)
        for lan in list_of_languages:
            language = lan.lang
            # print(lan.lang, lan.prob)
        return language


def likes_func(names):
    len_names = len(names)
    if len_names == 1:
        sms = '{name} likes this'.format(name=names[0])
    elif len_names == 2:
        sms = f'{names[0]} and {names[1]} like this'
    elif len_names == 3:
        sms = f'{names[0]}, {names[1]} and {names[2]} like this'
    elif len_names >= 4:
        sms = f'{names[0]}, {names[1]} and {len_names - 2} others like this'
    lang = lang_detection()
    translation = Translator(to_lang=lang)
    if len_names == 0:
        trans_sms = 'no one likes this'
    else:
        trans_sms = translation.translate(sms)
    return trans_sms


print(likes_func(names))
